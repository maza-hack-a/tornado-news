# Лента новостей
* Python tornado framework
* Motor mongodb driver
* Elasticsearch

# Склонировать репозиторий
    git clone https://USERNAME@bitbucket.org/maza-hack-a/tornado-news.git

# Установка Python плагинов
    pip install -r requirements.txt

# Запуск веб приложения
    python app.py

# Парсинг новостей из RSS ленты Point.md
    python scripts/parser.py

Ожидается что сервисы MongoDB, Elasticsearch уже установлены и запущены на момент запуска веб приложения.