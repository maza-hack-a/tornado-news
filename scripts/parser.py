import motor
from elasticsearch import Elasticsearch
import unidecode
import feedparser
import re
from datetime import datetime

db = motor.motor_tornado.MotorClient().news_database
es = Elasticsearch([{'host': 'localhost', 'port': 9200}])

parse_result = feedparser.parse('https://point.md/ru/rss/novosti/')

for entry in parse_result.entries:
	post = {
        "title": entry.title,
        "body":  re.sub("<.*?>", " ", entry.summary),
        "image": re.search(r'src="(.*?)"', entry.summary).group(1),
        "slug": re.sub(r'\W+', '_', unidecode.unidecode(entry.title).lower()),
        "date": datetime.strptime(entry.published, '%a, %d %b %Y %H:%M:%S GMT')
	}

	result = db.news.insert(post)
	es.index(index='news', doc_type='post', id=str(result), body={
            'title': post['title'],
            'body': post['body'],
            'slug': post['slug'],
            'image': post['image'],
            'date': post['date']
        })
