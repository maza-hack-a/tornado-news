# coding: utf-8

import tornado.web
from tornado import gen
import bleach, datetime, paginate, unidecode, re
from paginate import Page, make_html_tag
from urlparse import urlparse

class RootHandler(tornado.web.RequestHandler):
    def get(self):
        self.redirect(self.reverse_url("news", ''))

class NewsHandler(tornado.web.RequestHandler):
    @gen.coroutine
    def get(self, slug):
        db = self.settings['db']
        es = self.settings['es']
        if not slug:
            # Список новостей
            page = int(self.get_argument("page", 1))
            per_page = self.get_argument("per_page", 20)
            news = yield db.news.find().sort([('date', -1)]).skip((page-1)*per_page).to_list(length=per_page)
            news_count = yield db.news.count()
            p = paginate.Page([], page=page, items_per_page=per_page, item_count=news_count)
            pattern = u'$link_first $link_previous ~4~ $link_next $link_last<br><br> (Страница $page из $page_count. Количество записей: $item_count)'
            paginator_html = p.pager(pattern, url='/news/?page=$page', curpage_attr={'class':"active_page"}, link_tag=self.get_paginator_tag)
            self.render("newslist.html", news=news, paginatorHtml=paginator_html)
        else:
            try:
                # Страница отдельной новости
                post = yield db.news.find_one({'slug': slug})
                self.render("post.html", post=post)
            except Exception, err:
                raise tornado.web.HTTPError(404, err)

    def post(self):
        # Страница поиска новостей
        es = self.settings['es']
        search_text = self.get_argument("search_text", '')
        if not search_text:
            self.redirect(self.reverse_url("news", ''))
        else:
            result = es.search(index="news", body={"query": {"multi_match": {'fields': ['title^2', 'body^2'], 'query': search_text}}})
            self.render("search.html", news=result['hits'], searchRequest=search_text)

    def get_paginator_tag(self, item):
        return make_html_tag('li', 
            make_html_tag('a', 
                item['value'], 
                **{
                    'href': item['href'],
                    'class': "active_page" if item['type'] == 'current_page' else ''
                }
            )
        )

class PostHandler(tornado.web.RequestHandler):
    '''
        Добавление новости
    '''
    def get(self):
        db = self.settings['db']
        self.render("post_add.html", error=None)

    @gen.coroutine
    def post(self):
        db = self.settings['db']
        es = self.settings['es']

        # Загрузка изображения
        file_path = None

        if 'image' in self.request.files: 
            image_file = self.request.files['image'][0]
            file_path = "static/public/" + image_file['filename']
            output_file = open(file_path, 'wb')
            output_file.write(image_file['body'])

        formData = {
            "title": self.get_argument("title"),
            "image": file_path,
            "body": self.get_argument("message")
        }

        if not formData['title']:
            self.render("post_add.html", error="Не указан заголовок новости")
        elif not formData['image']:
            self.render("post_add.html", error="Не указан путь к изображению новости")
        elif not formData['body']:
            self.render("post_add.html", error="Не указан текст новости")
        else:
            VALID_TAGS = ['p', 'b', 'i', 'strong', 'em', 'img', 'iframe']
            post = {
                "title": formData['title'],
                "body": bleach.clean(formData['body'], tags=VALID_TAGS, 
                    attributes={'img': 'src', 'iframe': self.check_src}),
                "image": "/" + formData['image'],
                "slug": re.sub(r'\W+', '_', unidecode.unidecode(formData['title']).lower()),
                "date": datetime.datetime.now()
            }
            result = yield db.news.insert(post)
            es.index(index='news', doc_type='post', id=str(result), body={
                    'title': post['title'],
                    'body': post['body'],
                    'slug': post['slug'],
                    'image': post['image'],
                    'date': post['date']
                })
            self.redirect(self.reverse_url("news", '')+post['slug'])
            
    def check_src(self, tag, name, value):
        if name == 'src':
            p = urlparse(value)
            return (not p.netloc) or (p.netloc.replace('www.', '') in ['youtube.com', 'play.md', 'vimeo.com'])
        return False