# coding: utf-8

import tornado.ioloop
import tornado.httpserver
from tornado.options import define, options
from tornado.web import URLSpec as URL
import tornado
import os.path
import handlers
import motor
from elasticsearch import Elasticsearch

define("address", default="127.0.0.1")
define("port", default="8000")

urls = [
    URL("/", handlers.RootHandler, name='main'),
    URL("/add", handlers.PostHandler, name='post_add'),
    URL(r"/search", handlers.NewsHandler, name='search'),
    URL(r"/news/(.*?)", handlers.NewsHandler, name='news')
]

settings = {
    "template_path": os.path.join(os.path.dirname(__file__), "templates"),
    "static_path": os.path.join(os.path.dirname(__file__), "static"),
    "db": motor.motor_tornado.MotorClient().news_database,
    "es": Elasticsearch([{'host': 'localhost', 'port': 9200}]),
    "debug": True
}

def main():
    application = tornado.web.Application(urls, **settings)
    http_server = tornado.httpserver.HTTPServer(application)
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()

def check_es_index(es):
    if not es.indices.exists('news'):
        es.indices.create(index='news', body={
            'settings': {
                'analysis': {
                    'analyzer': {
                        'ru': {
                            'type': 'custom',
                            'tokenizer': 'standard',
                             'filter': ['lowercase', 'russian_morphology', 'ru_stopwords', 'ru_stemming'],
                        },
                        'ro': {
                            'type': 'custom',
                            'tokenizer': 'standard',
                            "filter": ['lowercase', 'ro_stemming', 'ro_stopwords'] 
                        }
                    },
                    'filter': {
                        'ru_stemming': {
                            'type': 'snowball',
                            'language': 'russian',
                        },
                        'ru_stopwords': {
                            'type': 'stop',
                            'stopwords': u'а,без,более,бы,был,была,были,было,быть,в,вам,вас,весь,во,вот,все,всего,всех,вы,где,да,даже,для,до,его,ее,если,есть,еще,же,за,здесь,и,из,или,им,их,к,как,ко,когда,кто,ли,либо,мне,может,мы,на,надо,наш,не,него,нее,нет,ни,них,но,ну,о,об,однако,он,она,они,оно,от,очень,по,под,при,с,со,так,также,такой,там,те,тем,то,того,тоже,той,только,том,ты,у,уже,хотя,чего,чей,чем,что,чтобы,чье,чья,эта,эти,это,я',
                        },
                        'ro_stemming': {
                            'type': 'snowball',
                            'language': 'romanian',
                        },
                        'ro_stopwords': {
                            'type': 'stop',
                            'stopwords': u'a,abia,acea,aceasta,această,aceea,aceeasi,acei,aceia,acel,acela,acelasi,acele,acelea,acest,acesta,aceste,acestea,acestei,acestia,acestui,aceşti,aceştia,acolo,acord,acum,adica,ai,aia,aibă,aici,aiurea,al,ala,alaturi,ale,alea,alt,alta,altceva,altcineva,alte,altfel,alti,altii,altul,am,anume,apoi,ar,are,as,asa,asemenea,asta,astazi,astea,astfel,astăzi,asupra,atare,atat,atata,atatea,atatia,ati,atit,atita,atitea,atitia,atunci,au,avea,avem,aveţi,avut,azi,aş,aşadar,aţi,b,ba,bine,bucur,bună,c,ca,cam,cand,capat,care,careia,carora,caruia,cat,catre,caut,ce,cea,ceea,cei,ceilalti,cel,cele,celor,ceva,chiar,ci,cinci,cind,cine,cineva,cit,cita,cite,citeva,citi,citiva,conform,contra,cu,cui,cum,cumva,curând,curînd,când,cât,câte,câtva,câţi,cînd,cît,cîte,cîtva,cîţi,că,căci,cărei,căror,cărui,către,d,da,daca,dacă,dar,dat,datorită,dată,dau,de,deasupra,deci,decit,degraba,deja,deoarece,departe,desi,despre,deşi,din,dinaintea,dintr,dintr-,dintre,doar,doi,doilea,două,drept,dupa,după,dă,e,ea,ei,el,ele,era,eram,este,eu,exact,eşti,f,face,fara,fata,fel,fi,fie,fiecare,fii,fim,fiu,fiţi,foarte,fost,frumos,fără,g,geaba,graţie,h,halbă,i,ia,iar,ieri,ii,il,imi,in,inainte,inapoi,inca,incit,insa,intr,intre,isi,iti,j,k,l,la,le,li,lor,lui,lângă,lîngă,m,ma,mai,mare,mea,mei,mele,mereu,meu,mi,mie,mine,mod,mult,multa,multe,multi,multă,mulţi,mulţumesc,mâine,mîine,mă,n,ne,nevoie,ni,nici,niciodata,nicăieri,nimeni,nimeri,nimic,niste,nişte,noastre,noastră,noi,noroc,nostri,nostru,nou,noua,nouă,noştri,nu,numai,o,opt,or,ori,oricare,orice,oricine,oricum,oricând,oricât,oricînd,oricît,oriunde,p,pai,parca,patra,patru,patrulea,pe,pentru,peste,pic,pina,plus,poate,pot,prea,prima,primul,prin,printr-,putini,puţin,puţina,puţină,până,pînă,r,rog,s,sa,sa-mi,sa-ti,sai,sale,sau,se,si,sint,sintem,spate,spre,sub,sunt,suntem,sunteţi,sus,sută,sînt,sîntem,sînteţi,să,săi,său,t,ta,tale,te,ti,timp,tine,toata,toate,toată,tocmai,tot,toti,totul,totusi,totuşi,toţi,trei,treia,treilea,tu,tuturor,tăi,tău,u,ul,ului,un,una,unde,undeva,unei,uneia,unele,uneori,unii,unor,unora,unu,unui,unuia,unul,v,va,vi,voastre,voastră,voi,vom,vor,vostru,vouă,voştri,vreme,vreo,vreun,vă,x,z,zece,zero,zi,zice,îi,îl,îmi,împotriva,în,înainte,înaintea,încotro,încât,încît,între,întrucât,întrucît,îţi,ăla,ălea,ăsta,ăstea,ăştia,şapte,şase,şi,ştiu,ţi,ţie'
                        }
                    }
                }
            }
        })
        es.indices.put_mapping(index='news', doc_type='post', body={
            'post': {
                'properties': {
                    'title': {
                        'type': 'string',
                        'fields': {
                            'ru': {
                                'type': 'string',
                                'analyzer': 'ru'
                            },
                            'ro': {
                                'type': 'string',
                                'analyzer': 'ro'
                            }
                        }
                    },
                    'body': {
                        'type': 'string',
                        'fields': {
                            'ru': {
                                'type': 'string',
                                'analyzer': 'ru'
                            },
                            'ro': {
                                'type': 'string',
                                'analyzer': 'ro'
                            }
                        }
                    }
                }
            }
        })
        print '[ElasticSearch] News index initialized'

if __name__ == "__main__":
    check_es_index(settings['es'])
    main()
