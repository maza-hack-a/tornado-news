tornado==4.4.3
motor==1.1
elasticsearch==5.3.0
Unidecode==0.4.20
paginate==0.5.6
bleach==2.0.0
feedparser==5.2.1